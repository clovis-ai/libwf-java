/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package posts

import TestUtils.runTest
import areas.sampleArea
import testingApi
import kotlin.test.*

class PostTest {

    @Test
    fun get_all_posts() = runTest {
        val sampleArea = sampleArea(testingApi())

        val posts = sampleArea.posts.nextPosts()

        assertEquals(2, posts.size)

        with(posts.find { it.author()?.id == 1 }) {
            assertNotNull(this)
            assertFalse(anonymous)
            assertTrue(subscribed)
            assertTrue(active)
            assertTrue(created.isNotBlank())
            assertTrue(text.startsWith("Post by admin"))
            assertTrue(text.contains("Lorem ipsum"))
            assertNull(mainImage)
            assertTrue(secondaryImages.isEmpty())
        }

        with(posts.find { it.author() == null }) {
            assertNotNull(this)
            assertTrue(anonymous)
            assertTrue(subscribed)
            assertTrue(created.isNotBlank())
            assertTrue(active)
            assertEquals("Example anon post by admin user.", text)
            assertTrue(
                mainImage?.encodedPath?.startsWith("/media/images/") ?: false,
                "Unexpected path: ${mainImage?.encodedPath}"
            )
            assertTrue(secondaryImages.isEmpty())

            assertEquals(2, comments.size)
            with(comments[0]) {
                assertEquals("admin", author().username)
                assertTrue(created.isNotBlank())
                assertEquals("Hey, who are you to claim that you are me.", text)
                assertNull(image)
            }
            with(comments[1]) {
                assertEquals("user", author().username)
                assertTrue(created.isNotBlank())
                assertEquals("It would be funny, if it were actually you.", text)
                assertNull(image)
            }
        }
    }

    @Test
    fun get_my_posts() = runTest {
        val api = testingApi()
        val sampleArea = sampleArea(api)

        val posts = sampleArea.posts.myPosts()

        assertEquals(2, posts.size)
        posts.forEach {
            if (!it.anonymous) assertEquals(api.me(), it.author())
            else assertNull(it.author())
        }
    }

    @Test
    fun subscribe() = runTest {
        var post = sampleArea(testingApi()).posts.nextPosts().find { it.author() == null }!!

        post.subscribe()
        post = post.getNewVersion()
        assertTrue(post.subscribed)

        post.unsubscribe()
        post = post.getNewVersion()
        assertFalse(post.subscribed)

        post.subscribe()
        post = post.getNewVersion()
        assertTrue(post.subscribed)
    }

}
