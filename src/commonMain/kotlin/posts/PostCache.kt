/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.posts

import io.ktor.http.HttpMethod
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.api.ApiPostRecursive
import net.wildfyre.lib.api.ApiPostsRecursive
import net.wildfyre.lib.api.ApiUser
import net.wildfyre.lib.api.toNonRecursive
import net.wildfyre.lib.users.User
import net.wildfyre.lib.utils.Cache
import net.wildfyre.lib.utils.SemaphoreCache

class PostCache(private val wildfyre: WildFyre, internal val area: String) :
    Cache<Int, PostData> by SemaphoreCache(wildfyre, { id ->
        val post = wildfyre.api.request<ApiPostRecursive>(HttpMethod.Get, "/areas/$area/$id/")
        Post(wildfyre, wildfyre.areas.get(area)!!.posts, post.toNonRecursive())
    }) {

    suspend fun nextPosts() = wildfyre.api.request<ApiPostsRecursive>(
        HttpMethod.Get, "/areas/$area/"
    ).toUsers()

    suspend fun myPosts() = wildfyre.api.request<ApiPostsRecursive>(HttpMethod.Get, "/areas/$area/own/").toUsers()

    private suspend fun ApiPostsRecursive.toUsers(): List<Post> {
        val newUsers = mutableSetOf<ApiUser>()

        val results = results.map { post ->
            post.author?.let { newUsers += it }
            post.comments.forEach { newUsers += it.author }
            Post(wildfyre, this@PostCache, post.toNonRecursive()).also { forceAddToCache(it.id, it) }
        }

        newUsers.forEach { wildfyre.users.forceAddToCache(it.user, User(wildfyre, it)) }

        return results
    }

    //TODO in T404: search for posts you subscribed to
}
