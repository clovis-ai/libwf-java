/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.posts

import io.ktor.http.Url
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.api.ApiComment

/**
 * Comments let users discuss [posts][Post].
 */
class Comment internal constructor(private val wildfyre: WildFyre, private val api: ApiComment) {

    /**
     * The comment's ID.
     */
    val id: Int
        get() = api.id

    /**
     * The author of this comment.
     */
    suspend fun author() = wildfyre.users.get(api.author)

    /**
     * The creation date of this comment.
     *
     * An example value is '2020-04-30T12:09:45.373523Z'.
     */
    val created: String
        get() = api.created

    /**
     * The text of this comment.
     *
     * The text can be written in MarkDown.
     */
    val text: String
        get() = api.text

    /**
     * The image of this comment, if any.
     */
    val image: Url?
        get() = api.image?.let { Url(it) }

    override fun toString() = api.toString()
}
