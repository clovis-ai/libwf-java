/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib

import io.ktor.http.HttpMethod
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import net.wildfyre.lib.account.Account
import net.wildfyre.lib.api.ApiAccount
import net.wildfyre.lib.api.ApiRegistration
import net.wildfyre.lib.api.ApiToken
import net.wildfyre.lib.api.ApiUsernamePassword
import net.wildfyre.lib.areas.Areas
import net.wildfyre.lib.http.ApiAccess
import net.wildfyre.lib.users.UserCache
import kotlin.js.JsName
import kotlin.jvm.JvmStatic

/**
 * The WildFyre class is used to instantiate the access to the API.
 *
 * You can get an object of this class via the factory methods [WildFyre.connect].
 */
// Factory methods instead of a constructor, because constructors cannot suspend.
class WildFyre private constructor(internal val api: ApiAccess, val config: WildFyreConfig) : CoroutineScope by CoroutineScope(SupervisorJob(config.job)) {

    private val anonymousApi = WildFyreAnonymous.connect(api.baseUrl)

    /**
     * Get access to your account and ways to modify it.
     */
    lateinit var account: Account
        private set

    /**
     * Allows to access information about the users.
     */
    val users = UserCache(this)

    /**
     * Allows to access information about the various areas.
     */
    val areas = Areas(this)

    /**
     * The token used by the library to access the API.
     *
     * This token, unlike the password, is safe to be stored, as the user can invalidate a previous
     * token. It is the responsibility of the client of this library to store it somewhere, and to
     * give it back to the library the next time so the password isn't needed.
     */
    val token: String
        get() = api.token
            ?: throw IllegalStateException("Missing token. This should not be happen, please report to the WildFyre Library maintainers.")

    suspend fun me() = account.me()

    /**
     * Disconnect from the API and cancel every currently running job.
     */
    fun disconnect() {
        config.job.cancel("The user has requested to disconnect from the API.")
    }

    companion object {

        /**
         * The URL of the official WildFyre servers.
         */
        const val PRODUCTION_API = "https://api.wildfyre.net"

        /**
         * The default URL of the development WildFyre API.
         */
        const val LOCAL_API = "http://localhost:8000"

        /**
         * Use this factory method to connect to the API with the user's [username] and [password].
         *
         * You can use [url] to connect to a different instance than the official servers. See [PRODUCTION_API] and
         * [LOCAL_API].
         *
         * As a client of this API, you should not store the password -- this API will not store it
         * either. On successful call of this method, you will be given a [WildFyre] object: use its
         * property [WildFyre.token] and save that. On the next connection to the service, use the
         * overload of this method that accepts that token.
         *
         * Additionnal settings, such has the number of parallel downloads, can be given with [config].
         */
        @JvmStatic
        @JsName("connectWithPassword")
        suspend fun connect(username: String, password: String, url: String = PRODUCTION_API, config: WildFyreConfig = DefaultConfig): WildFyre {
            val res = ApiAccess.anonymous(url).request<ApiToken>(HttpMethod.Post, "/account/auth/", json = ApiUsernamePassword(username, password))

            return connect(res.token, url, config)
        }

        /**
         * Use this factory method to connect to the API with the user's [token].
         *
         * You can use [url] to connect to a different instance than the official servers. See [PRODUCTION_API] and
         * [LOCAL_API]
         *
         * If you do not have a token yet, call the overload of this method, which will let you
         * connect without one. When you do, you are expected to get the token from that connection,
         * and then store it so you can call this method on subsequent initializations.
         *
         * Additionnal settings, such has the number of parallel downloads, can be given with [config].
         */
        @JvmStatic
        @JsName("connectWithToken")
        suspend fun connect(token: String, url: String = PRODUCTION_API, config: WildFyreConfig = DefaultConfig): WildFyre {
            val api = ApiAccess.authenticated(url, token)
            val res = api.request<ApiAccount>(HttpMethod.Get, "/account/")

            check(res.id >= 0) { "The user ID should not be negative: ${res.id}. Something went wrong." }

            return WildFyre(api, config).also { wildfyre ->
                wildfyre.account = Account(wildfyre, res)
            }
        }

        /**
         * Creates a new WildFyre account, and logs in.
         *
         * At this point, only the official server can issue a CAPTCHA ID, which means that this method can only be
         * used for testing purposes (the testing server does not check the validity of the captcha). If you are
         * making your own application, it is better to ask your user to connect via the official servers, and log in
         * in your app afterwards.
         */
        suspend fun createNewAccount(username: String, email: String, password: String, captcha: String, url: String = PRODUCTION_API, config: WildFyreConfig = DefaultConfig): WildFyre {
            val api = ApiAccess.anonymous(url)

            val res = api.request<ApiAccount>(HttpMethod.Post, "/account/register/", json = ApiRegistration(username, email, password, captcha))

            return connect(res.username, password, url, config)
        }
    }
}
