/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.areas

import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.api.ApiArea
import net.wildfyre.lib.posts.PostCache

/**
 * Areas allow to get access to [posts] and the user's [reputation] and [spread].
 *
 * To get the available areas, see [WildFyre.areas] and [Areas.getAll].
 */
class Area internal constructor(private val wildfyre: WildFyre, private val api: ApiArea) {

    /**
     * The ID of this area.
     */
    val id: String
        get() = api.name

    /**
     * The name that should be displayed to the user.
     */
    val displayName: String
        get() = api.displayname

    /**
     * The authenticated user's reputation in this area.
     *
     * The reputation can be seen as the user's XP; it increases or decreases
     * according to their actions.
     *
     * This value is cached; if you think it is not up-to-date, see [flagAsOutdated].
     *
     * See [spread] and [https://api.wildfyre.net/docs/areas/rep.html].
     */
    suspend fun reputation(): Int {
        return wildfyre.areas.reputations.get(id).reputation
    }

    /**
     * The authenticated user's spread in this area.
     *
     * The spread can be seen as the user's level; it increases or decreases
     * when their reputation reaches a milestone.
     *
     * The spread corresponds to how many people their actions will reach
     * (posting, extinguishing, igniting...).
     *
     * This value is cached; if you think it is not up-to-date, see [flagAsOutdated].
     *
     * See [reputation] and [https://api.wildfyre.net/docs/areas/rep.html].
     */
    suspend fun spread(): Int {
        return wildfyre.areas.reputations.get(id).spread
    }

    /**
     * Flags the [reputation] and [spread] as out-of-date. The next time they are
     * queried, newer information will be downloaded.
     */
    suspend fun flagAsOutdated() {
        wildfyre.areas.reputations.flagAsOutdated(id)
    }

    val posts = PostCache(wildfyre, id)
}
