/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib

import io.ktor.http.HttpMethod
import net.wildfyre.lib.http.ApiAccess
import kotlin.js.JsName
import kotlin.jvm.JvmStatic

/**
 * Allows the user of this library to perform unauthenticated actions on the WildFyre servers.
 *
 * Use [WildFyreAnonymous.connect] to instantiate this object.
 */
class WildFyreAnonymous private constructor(internal val api: ApiAccess) {

    suspend fun areas(): String = api.request(HttpMethod.Get, "/areas/")

    companion object {

        /**
         * Connect to the WildFyre API without authentication.
         */
        @JvmStatic
        @JsName("connect")
        fun connect(url: String = WildFyre.PRODUCTION_API) = WildFyreAnonymous(ApiAccess.anonymous(url))

    }

}
