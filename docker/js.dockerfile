FROM node:alpine
MAINTAINER WildFyre.net

# Dependencies of the scripts
RUN apk add --no-cache bash curl wget lsof git

# Dependencies of the API
RUN apk add --no-cache postgresql-dev gcc \
    libffi libffi-dev \
    musl-dev libc-dev \
    zlib zlib-dev \
    libjpeg jpeg jpeg-dev \
    python3 python3-dev py3-virtualenv

# Get Java
RUN apk add --no-cache openjdk11-jre openjdk11

# Get Chromium
RUN apk add --no-cache chromium
ENV CHROME_BIN=chromium-browser
ENV CHROMIUM_USER_FLAGS="--no-sandbox"
